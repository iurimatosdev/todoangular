import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Item } from "../shared/models/item";
import { StorageService } from "../shared/services/storage.service";

@Component({
  selector: "app-todo-item-list",
  templateUrl: "./item-list.component.html",
  styleUrls: ["./item-list.component.css"]
})
export class ItemListComponent {
  @Output() itemChanged: EventEmitter<any> = new EventEmitter();
  @Input() items: Item[];

  constructor(private storageService: StorageService) {}

  public changeItemStatus(item: Item): void {
    this.storageService.toggleItem(item.id);
    this.itemChanged.emit();
  }

  public removeItem(item: Item): void {
    this.storageService.removeItem(item.id);
    this.itemChanged.emit();
  }

  public totalCompleted(): number {
    return this.items.filter(cur => cur.done).length;
  }

  public totalRemaining(): number {
    return this.items.filter(cur => !cur.done).length;
  }
}
