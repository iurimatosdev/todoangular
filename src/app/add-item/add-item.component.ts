import { Component, EventEmitter, Output, ViewChild } from "@angular/core";
import { Item } from "../shared/models/item";
import { StorageService } from "../shared/services/storage.service";

@Component({
  selector: "app-todo-add-item",
  templateUrl: "./add-item.component.html",
  styleUrls: ["./add-item.component.css"]
})
export class AddItemComponent {
  @ViewChild("addItemForm") form: any;
  @Output() itemAdded: EventEmitter<any> = new EventEmitter();
  public task: string;

  constructor(private storageService: StorageService) {}

  public addItem({ value, valid }): void {
    console.log({ value, valid });

    if (valid) {
      const item = new Item(this.task);
      this.storageService.addItem(item);
      this.itemAdded.emit();
    }
  }
}
