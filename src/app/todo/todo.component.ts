import { Component, OnInit } from "@angular/core";
import { Item } from "../shared/models/item";
import { StorageService } from "../shared/services/storage.service";

@Component({
  selector: "app-todo",
  templateUrl: "./todo.component.html",
  styleUrls: ["./todo.component.css"]
})
export class TodoComponent implements OnInit {
  public items: Item[];

  constructor(private storageService: StorageService) {}

  ngOnInit() {
    this.updateItems();
  }

  public onItemAdded(): void {
    this.updateItems();
  }

  public onItemChanged(): void {
    this.updateItems();
  }

  private updateItems(): void {
    this.items = this.storageService.getItems().reverse();
  }
}
