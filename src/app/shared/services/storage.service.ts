import { Injectable } from "@angular/core";
import { Item } from "../models/item";

@Injectable({
  providedIn: "root"
})
export class StorageService {
  private storageKey = "todo-items-key";

  constructor() {}

  public addItem(item: Item): void {
    const items = this.getItems();
    items.push(item);
    this.saveItems(items);
  }

  public removeItem(id: string): void {
    const items = this.getItems();
    const index = items.findIndex(current => current.id === id);
    items.splice(index, 1);
    this.saveItems(items);
  }

  public toggleItem(id: string): void {
    const items = this.getItems();
    const item = items.find(current => current.id === id);
    item.done = !item.done;
    this.saveItems(items);
  }

  public getItems(): Item[] {
    const storageContent = localStorage.getItem(this.storageKey);
    if (storageContent === null) {
      return [];
    }

    const data: Item[] = JSON.parse(storageContent);
    return data;
  }

  private saveItems(items: Item[]) {
    localStorage.setItem(this.storageKey, JSON.stringify(items));
  }
}
