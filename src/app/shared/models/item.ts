export class Item {
  id: string;
  content: string;
  done: boolean;
  createdAt: Date;

  constructor(content: string) {
    this.id = this.generateId();
    this.content = content;
    this.createdAt = new Date();
    this.done = false;
  }

  private generateId(): string {
    return Math.random()
      .toString(36)
      .substr(2, 9)
      .toUpperCase();
  }
}
